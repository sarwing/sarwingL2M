from setuptools import setup 

setup(name='sarwingL2M',
      description='Sarwing mutli-resolution module',
      url='https://gitlab.ifremer.fr/sarwing/sarwingL2M.git',
      use_scm_version={'write_to': '%s/_version.py' % "sarwingL2M"},
      author = "Theo Cevaer",
      author_email = "Theo.Cevaer@ifremer.fr",
      license='GPL',
      packages=['sarwingL2M'],
      install_requires=[
          'numpy', 'shapely', 'netCDF4', 'scipy'
      ],
      zip_safe=False,
      scripts=['bin/swMultiResWrapper.sh',
               'bin/swMultiRes.py',
               'bin/swMultiResCreator.py',
               'bin/swMultiResListing.py'],
)
