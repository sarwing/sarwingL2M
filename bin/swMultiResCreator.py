#!/usr/bin/env python

from builtins import str
import logging
import argparse
import datetime
import sys
import os
import socket
import sarwingL2M


logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

logger.info("Script executed at %s" % datetime.datetime.now())
logger.info("Called with the following command : %s" % " ".join(sys.argv))
logger.info("The host machine is : %s" % socket.gethostname())

description = """Reads paths from stdin and creates new .nc files with the wanted resolutions.
The existing files are not recreated, they are skipped by default."""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("input_listing", nargs="?", type=argparse.FileType("r"), default=sys.stdin, help="The input listing file to read from. If not specified, the script will read from stdin.")
parser.add_argument("resolutions", action="store", nargs="+", type=int, help="list of resolutions. A new file will be created for each resolution")
parser.add_argument("-d", "--outDir", action="store", default=".", type=str,help="Base output directory (must exist) (other directories will be created inside)")
parser.add_argument("-f", "--force", action="store_true", default=False, help="Forces existing files to be recreated.")
parser.add_argument("--debug", action="store_true", default=False, help="start the script in debug mode")
parser.add_argument("--reportPath", action="store", default=None, help="Specify the path and filename where to record the created files")


args = parser.parse_args()

if args.debug:
    try:
        import debug
    except:
        pass    

l2cpath = args.input_listing.readlines()
l2cpath = [i.strip() for i in l2cpath]

try:
    sarwingL2M.swMultiResManager.createMultiRes(l2cpath, outDir=args.outDir, force=args.force, reportPath=args.reportPath, resolutions=args.resolutions, debug=args.debug)
except Exception as e:
    logger.exception("Exception : %s", str(e) )
    sys.exit(1)