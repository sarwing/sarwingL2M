#!/bin/bash

outDir=$1
owiFile=$2
res=$3
dir=`dirname $1`

mkdir -p "$dir"

swMultiRes.py $debug -d $outDir -i $owiFile -r $res > "$dir/L2M$res.log" 2>&1
status=$?
echo $status > "$dir/L2M$res.status"

swMultiRes.py -v > "$dir/L2M$res.version"

cat "$dir/L2M$res.log"

exit $status