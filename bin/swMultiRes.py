#!/usr/bin/env python

from __future__ import print_function
from builtins import str
import logging
import argparse
import datetime
import sys
import socket
import os
import sarwingL2M

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

description = """Lowers the resolution of the input file and saves it into a new file named with the given name"""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-i", "--inputFile", action="store", nargs=1, type=str, help="netCDF (.nc) file on which to work on")
parser.add_argument("-d", "--outDir", action="store", default=".", nargs=1, type=str, help="Base output directory (and filename if -a is not set) (non existing intermediary directories will be created)")
parser.add_argument("-r", "--resolution", action="store", nargs=1, type=int, help="Resolution desired at output (in kilometer)")
parser.add_argument("-a", "--autoName", action="store_true", default=False, help="If this flag is set, the script generates the output filename itself. ")
parser.add_argument("--debug", action="store_true", default=False, help="start the script in debug mode")
parser.add_argument("-v", "--version", action="store_true", default=False, help="Returns the script version")
args = parser.parse_args()

if args.version:
    print(sarwingL2M.swMultiResProcessor.version)
    sys.exit(0)
    
logger.info("Script executed at %s" % datetime.datetime.now())
logger.info("Called with the following command : %s" % " ".join(sys.argv))
logger.info("The host machine is : %s" % socket.gethostname())

if args.debug:
    try:
        import debug
    except:
        pass
    
    
if not os.path.exists(os.path.dirname(args.outDir[0])):
    try:
        os.makedirs(os.path.dirname(args.outDir[0]))
    except:
        pass

try:
    sarwingL2M.swMultiResProcessor.multiRes(args.inputFile[0], args.outDir[0], args.resolution[0], args.autoName)
except Exception as e:
    logger.exception("Exception : %s", str(e) )
    sys.exit(1)