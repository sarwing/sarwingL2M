#!/usr/bin/env python

import logging
import argparse
import os
import sys
import datetime
import socket
from subprocess import call

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

qsubarray=os.getenv("QSUBARRAY", "")

logger.info("Script executed at %s" % datetime.datetime.now())
logger.info("Called with the following command : %s" % " ".join(sys.argv))
logger.info("The host machine is : %s" % socket.gethostname())

description = """Calls the following command : swMultiResCreator.py -d outDir --reportPath reportPath [debug] [force] res1 res2 ... | qsubarray xargs -l1 -r swMultiResWrapper.sh ...
This script is made to be used with pipe : cat listing.txt | swMultiResListing.py args..."""
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-d", "--outDir", action="store", default=".", type=str,help="Base output directory (other directories will be created inside)")
parser.add_argument("-f", "--force", action="store_const", const="--force", default="", help="Forces existing files to be recreated.")
parser.add_argument("--debug", action="store_const", const="--debug", default="", help="Starts the script in debug mode")
parser.add_argument("--reportPath", action="store", default=None, help="Specify the path where to record the created report files")
parser.add_argument("resolutions", action="store", nargs="+", type=str, help="list of resolutions. A new file will be created for each resolution")

args = parser.parse_args()

if args.debug:
    try:
        import debug
    except:
        pass
    
resos = ' '.join(args.resolutions)
    
logger.info("Starting multi-resolutions listing process...")
call('swMultiResCreator.py -d %s --reportPath %s %s %s %s |%s xargs -I CMD -r bash -c "swMultiResWrapper.sh CMD"' % (args.outDir, args.reportPath, args.debug, args.force, resos, qsubarray), 
     shell=True, executable="/bin/bash")
logger.info("Finished multi-resolutions listing process.")
