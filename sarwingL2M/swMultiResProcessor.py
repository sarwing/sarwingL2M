#!/usr/bin/env python

from __future__ import division
from builtins import str
from builtins import range
from past.utils import old_div
import netCDF4
import numpy as np
import os
import logging
import argparse
from scipy.ndimage.filters import generic_filter
from shutil import copyfile
import math
import sys
import datetime
from ._version import __version__

version = __version__

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

#See multiResWrapper.sh for easier usage

def generateFilename(inputFile, resolution):
    splitPath = inputFile.split("/")
    owiSplit = splitPath[-1].split("-")
    origLen = len(owiSplit[-2])
    owiSplit[-2] = str(resolution)
    owiSplit[-2] = owiSplit[-2].zfill(origLen)
    
    owicc = list(owiSplit[3])
    owicc[1] = 'm'
    owiSplit[3] = "".join(owicc)
    
    return "-".join(owiSplit)
    

# Generate netCDF file with lower resolution from a netCDF source file.
def multiRes(inputFile, outputFile, resolution, autoName):
    logger.info("Starting multi-resolution process on input file %s. Will generate files for the following resolution : %s" % (inputFile, resolution))

    # Lower resolution will be applied on these variables
    realVars = ['owiWindSpeed','owiInversionTables_UV/owiWindSpeed_Tab_dualpol_2steps', 'owiNrcs_no_noise_correction', 'owiNrcs',
                 'owiNrcs_cross_no_noise_correction', 'owiNrcs_cross', 'owiWindSpeed_IPF', 'owiEcmwfWindSpeed']#'owiInversionTables_UV/owiWindSpeed_Tab_copol', 

    if autoName:
        outputFile = outputFile + "/" + generateFilename(inputFile, resolution)
    
    if not os.path.exists(os.path.dirname(outputFile)):
        try:
            os.makedirs(os.path.dirname(outputFile))
        except:
            pass
    logger.info("Output path and filename : %s" % (outputFile))
    
    copyfile(inputFile, outputFile)

    try:
        ncIn = netCDF4.Dataset(inputFile,'r')
    except Exception as e:
        logger.error('cannot open file %s : %s. Exiting' % (inputFile,  str(e)))
        sys.exit(1)
    
    try:
        ncOut = netCDF4.Dataset(outputFile, 'r+')
    except Exception as e:
        logger.error('cannot open file %s : %s. Exiting' % (outputFile,  str(e)))
        sys.exit(1)

    # Setting processor version to output product
    setattr(ncOut, "multi_resolution_processor_version", version)

    logger.debug("Generating footprint...")
    
    # The footprint is circle-like shape. Its radius depends on the wanted resolution
    # Each data point will be assigned as value the mean of the other data within the footprint
    footprint = np.ones((resolution, resolution))
    a, b = old_div(resolution, 2), old_div(resolution, 2)
    r = old_div(resolution, 2)
    if resolution % 2 == 0:
        a = a - 0.5
        b = b - 0.5
        r = r - 0.5
    epsilon = resolution * (30.0/100.0)
    for x in range(0, len(footprint)):
        for y in range(0, len(footprint[x])):
            if ((x-a)**2 + (y-b)**2) >= r**2 + epsilon:
                footprint[x][y] = 0
                
    logger.debug("Finished generating footprint.")
    
    #Getting a mask to ignore invalid data
    mask = np.ma.getmask(np.ma.masked_greater(ncIn['owiMask'], 0))
    
    logger.debug("Starting multi-res computing on each selected var...")

    #Computing lower resolution for each var
    for varName in realVars:
        groupName = '/'
        var = varName
        if '/' in varName:
            groupName = varName.split('/')[0]
            var = varName.split('/')[1]

        if var in ncIn.variables or groupName in ncIn.groups and var in ncIn.groups[groupName].variables:
            logger.debug("Computing for var %s..." % varName)
            var = np.ma.array(ncIn[varName][:], mask=mask)
            var = np.ma.masked_values(var, float(-9999.0)) # Expanding max to the default value -9999.0
            mask = np.ma.getmask(var)
            var[mask] = np.nan#Setting nan to each masked value. Useful for nanmean to ignore nan values when computing the mean
            arrayOut = np.empty_like(ncIn[varName][:])
            generic_filter(var, mean, footprint=footprint, mode='constant', output=arrayOut, cval=np.nan)#Computing lower resolution
            ncOut[varName][:] = arrayOut
            logger.debug("Computing for var %s is over." % varName)
        else:
            logger.warning("Variable %s not found, skipping." % varName)

        
    logger.debug("Finished to compute multi-res data. Closing files...")
    
    ncIn.close()
    ncOut.close()
    
    logger.debug("Files closed. Exiting.")
                                             
def mean(subArray):
    
    return np.nanmean(subArray)
