from __future__ import print_function
from __future__ import absolute_import
from builtins import str
import sys
import os
import argparse
import re
import logging
import csv
from .swMultiResProcessor import version
import datetime


logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

def checkVersion(outDir, res):
    filen = "%s/L2M%s.version" % (outDir, res)
    try:
        logger.debug("Trying to open version file in %s for resolution %s..." % (outDir, res))
        f = open(filen, "r")
    except IOError:
        logger.debug("No previous version file. Generation will be forced.")
        return True
    else:
        with f:
            ver = f.readline().strip()
            if ver == version:
                logger.debug("Version file open, versions are the same : %s. Generation will not be forced" % ver)
                return False
            else:
                logger.debug("Version file open, versions are different, previous : %s, newest : %s. Generation will be forced." % (ver.strip(), version.strip()))
                return True
            
def checkStatus(outDir, res):
    filen = "%s/L2M%s.status" % (outDir, res)
    try:
        logger.debug("Trying to open status file in %s for resolution %s..." % (outDir, res))
        f = open(filen, "r")
    except IOError:
        logger.debug("No previous status file. Generation will be forced.")
        return True
    else:
        with f:
            status = f.readline().strip()
            if status == "0":
                logger.debug("Status OK")
                return False
            else:
                logger.debug("Status is %s, generation will be forced" % status)
                return True 

def createMultiRes(l2path, outDir=".", force=False, reportPath=None, debug=False, resolutions=[]):
    if reportPath:
        logFileHandler = logging.FileHandler(reportPath + '/swCreateMultiRes.log')
        logger.addHandler(logFileHandler)

    logger.info(f"Processing {len(l2path)} files, at resolutions {resolutions} in {outDir}")
    
    reportFile = None
    fieldnames = ['Comments', 'L2M file', 'Resolution', 'L2X file']
    if reportPath:
        if os.path.exists(reportPath):
            reportFilepath = "%s/multiResReport.csv" % reportPath
            try:
                reportFile = open(reportFilepath, "w+")
                logger.info("File %s created (CSV report file)" % reportFilepath)
            except IOError:
                logger.warning("Error: could not open report file: %s" % reportFilepath)
                
            writer = csv.DictWriter(reportFile, fieldnames=fieldnames)
            writer.writeheader()

            l2m_out_files = "%s/l2m_out_files.txt" % reportPath
            try:
                l2m_out_files_file = open(l2m_out_files, "w+")
                logger.info("File %s created (L2M outfiles file)" % l2m_out_files)
            except IOError:
                logger.warning("Error: could not open L2M outfiles file: %s" % l2m_out_files)

            l2m_outdirs = "%s/l2m_out_dirs.txt" % reportPath
            try:
                l2m_outdirs_file = open(l2m_outdirs, "w+")
                logger.info("File %s created (L2M outdirs file)" % l2m_outdirs)
            except IOError:
                logger.warning("Error: could not open L2M outdirs file: %s" % l2m_outdirs)

            l2m_cmd = "%s/l2m_cmd.txt" % reportPath
            try:
                l2m_cmd_file = open(l2m_cmd, "w+")
                logger.info("File %s created (L2M cmd file)" % l2m_cmd)
            except IOError:
                logger.warning("Error: could not open L2M cmd file: %s" % l2m_cmd)
    
    # Exemple of l2path: 
    # /home3/homedir11/perso/tcevaer/datawork/tmp/chain_processing/processings/
    # xsar_2024v10v21_1v6_0v2v6_1v1v4_1v0v6_1v0v6_1v2v4_1v0v3_1v0v1_1v2v1/config_example/sentinel-1a/
    # L2C/IW/S1A_IW_GRDH_1S/2023/297/S1A_IW_GRDH_1SDV_20231024T004708_20231024T004802_050900_0622AE_0CC7X3F98/s1a-iw-owi-cc-20231024t004708-20231024t004802-050900-0622AE.nc
    for owiFileCom in l2path:
        rs2Listing = "RS2" in owiFileCom
        rcmListing = "rcm" in owiFileCom.lower()
        
        owiPath = owiFileCom.split(" ")[0].strip()
        commentList = owiFileCom.split(" ")[1:]
        comment = " ".join(commentList)

        pathSplit = owiPath.split("/")
        
        # Replace L2C by L2M in output path
        if re.match("L\d\w?", pathSplit[-7]):
            pathSplit[-7] = "L2M"
        else:
            raise Exception("L2* directory not found in path %s at index -7" % pathSplit)

        #Create symbolic link between L2CPath/post_processing/L2M and L2M path
        inputL2Path = os.path.dirname(owiPath)
        outputL2MPath = os.path.dirname("/".join(pathSplit))
        L2PostprocPath = os.path.join(inputL2Path, "post_processing")
        # If the L2M plugin is not run in standalone mode but following concatenation
        if "/L2C/" in inputL2Path:
            if not os.path.exists(L2PostprocPath):
                logger.debug("Creating post_process path %s" % L2PostprocPath)
                os.makedirs(L2PostprocPath)
            linkDest = os.path.join(L2PostprocPath, "L2M")
            # If link exists and is not what's expected
            if os.path.lexists(linkDest) and os.readlink(linkDest) != outputL2MPath:
                logger.info("Removing old symbolic link pointing to wrong directory. %s" % linkDest)
                os.path.unlink(linkDest)
            if not os.path.lexists(linkDest):
                logger.info("Creating symbolic link %s to %s" % (outputL2MPath, linkDest))
                os.symlink(outputL2MPath, linkDest)
            
            
        fullPath = ""
        for reso in resolutions:
            owiSplit = pathSplit[-1].split("-") # s1a-iw-owi-cc-20231024t004708-20231024t004802-050900-0622AE.nc
            origLen = len(owiSplit[-2]) #  050900
            owiSplit[-2] = str(reso) # ex: 3
            owiSplit[-2] = owiSplit[-2].zfill(origLen) # 000003
        
            owicc = list(owiSplit[3]) # cc (as list)
            owicc[1] = 'm' # cm
            owiSplit[3] = "".join(owicc) # cm
            pathSplit[-1] = "-".join(owiSplit) # s1a-iw-owi-cm-20231024t004708-20231024t004802-000003-0622AE.nc
            
            endPath = "/".join(pathSplit[-8:]) # S1A/L2M/IW/S1A_IW_GRDH_1S/2023/297/S1A_IW_GRDH_1SDV_20231024T004708_20231024T004802_050900_0622AE_0CC7X3F98/s1a-iw-owi-cm-20231024t004708-20231024t004802-000003-0622AE.nc
            fullPath = outDir + "/" + endPath
            
            # To have filename as file result in the report
            if reportFile: 
                writer = csv.DictWriter(reportFile, fieldnames=fieldnames)
                writer.writerow({'Comments': comment.strip(), 'L2M file': fullPath, 'Resolution': reso, 'L2X file': owiPath})

                if l2m_out_files_file:
                    l2m_out_files_file.write(f"{fullPath}\n")
                
                if l2m_outdirs_file:
                    l2m_outdirs_file.write(f"{os.path.dirname(fullPath)}\n")

            oDir = os.path.dirname(fullPath)
            if force or checkStatus(oDir, reso) or checkVersion(oDir, reso) or not os.path.isfile(fullPath):
                if reportFile:
                    # Write to the file after the status check to avoid re-submitting already processed tasks.
                    if l2m_cmd_file:
                        l2m_cmd_file.write(f"swMultiResWrapper.sh {fullPath} {owiPath} {reso}\n")

                sys.stdout.write(f"{fullPath} {owiPath} {reso}\n")
                #print("%s %s %s" % (fullPath, owiPath, reso))
            else:
                logger.debug("File %s already exists, skipping" % fullPath)  
        
        #if reportFile: 
        #    writer = csv.DictWriter(reportFile, fieldnames=fieldnames)
        #    writer.writerow({'Comments': comment.strip(), 'L2M directory': os.path.dirname(fullPath), 'L2X file': owiPath})                              
    if reportFile:
        reportFile.close()
    
